package de.rwth.swc.sqa;

import de.rwth.swc.sqa.model.TicketRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class TicketBuyingIntegrationTest {
    @LocalServerPort
    private int port;

    private static Collection<Arguments> provideAllowedTicketRequests() {

        // everything is allowed except student on 1H and 1D tickets
        // tickets without zone information are allowed to have any zone or no zone

        List<Arguments> args = new ArrayList<>();
        TicketRequest.ValidForEnum[] longValidFor = {TicketRequest.ValidForEnum._30D, TicketRequest.ValidForEnum._1Y};
        TicketRequest.ZoneEnum[] allZone = {TicketRequest.ZoneEnum.A, TicketRequest.ZoneEnum.B, TicketRequest.ZoneEnum.C};
        boolean[] bools = {true, false};
        Integer[] allDiscount = {null, 25, 50};

        // no student tickets are issued for 1h / 1d tickets
        for (var discount : allDiscount) {
            for (var disabled : bools) {
                for (var zone : allZone) {
                    // valid 1h ticket has a zone
                    TicketRequest ticketRequest = new TicketRequest().validFrom("2022-05-16T14:13:12")
                            .birthdate("1999-09-19").student(false).discountCard(discount)
                            .validFor(TicketRequest.ValidForEnum._1H).zone(zone)
                            .disabled(disabled);
                    args.add(Arguments.of(ticketRequest));
                }
                // valid 1d ticket has no zone
                TicketRequest ticketRequest2 = new TicketRequest().validFrom("2022-05-16T14:13:12")
                        .birthdate("1999-09-19").student(false).discountCard(discount)
                        .validFor(TicketRequest.ValidForEnum._1D)
                        .disabled(disabled);
                args.add(Arguments.of(ticketRequest2));
            }
        }

        // student tickets are issued for 30D/1Y tickets, these tickets have no zone
        for (var validFor : longValidFor) {
            for (var disabled : bools) {
                for (var student : bools) {
                    for (var discount : allDiscount) {
                        TicketRequest ticketRequest = new TicketRequest().validFrom("2022-05-16T14:13:12")
                                .birthdate("1999-09-19").validFor(validFor)
                                .disabled(disabled).student(student).discountCard(discount);
                        args.add(Arguments.of(ticketRequest));
                    }
                }
            }
        }

        return args;
    }

    public static Collection<Arguments> provideDisallowedTicketRequests() {
        List<Arguments> args = new ArrayList<>();
        TicketRequest.ValidForEnum[] shortValidFor = {TicketRequest.ValidForEnum._1H, TicketRequest.ValidForEnum._1D};
        TicketRequest.ZoneEnum[] zoneEnums = {TicketRequest.ZoneEnum.A, TicketRequest.ZoneEnum.B, TicketRequest.ZoneEnum.C};
        boolean[] booleans = {true, false};
        int[] allDiscount = {25, 50};

        // it is not allowed to have student flag on 1H and 1D tickets
        for (var validFor : shortValidFor) {
            for (var zone : zoneEnums) {
                for (var disabled : booleans) {
                    for (var discount : allDiscount) {
                        TicketRequest ticketRequest = new TicketRequest().validFrom("2022-05-16T14:13:12")
                                .birthdate("1999-09-19").validFor(validFor).zone(zone).
                                disabled(disabled).discountCard(discount).student(true);
                        args.add(Arguments.of(ticketRequest));
                    }
                }
            }
        }

        TicketRequest.ValidForEnum[] non1HTickets = {
                TicketRequest.ValidForEnum._1D,
                TicketRequest.ValidForEnum._30D,
                TicketRequest.ValidForEnum._1Y
        };
        // non 1H tickets are not allowed to have a zone
        for (var validFor : non1HTickets) {
            for (var zone : zoneEnums) {
                for (var disabled : booleans) {
                    for (var student : booleans) {
                        for (var discount : allDiscount) {
                            TicketRequest ticketRequest = new TicketRequest().validFrom("2022-05-16T14:13:12")
                                    .birthdate("1999-09-19").validFor(validFor).zone(zone).
                                    disabled(disabled).discountCard(discount).student(student);
                            args.add(Arguments.of(ticketRequest));
                        }
                    }
                }
            }
        }

        return args;
    }

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @ParameterizedTest
    @CsvSource({
            "false,false",
            "false,true",
            "true,false",
            "true,true"
    })
    void whenFieldsAreMissing_thenDefaultAreCorrect(Boolean hasDisabledField, Boolean hasStudentField) {
        JSONObject data = new JSONObject();

        try {
            data.put("validFrom", "2020-10-29T10:38:59");
            data.put("birthdate", "2001-01-01");
            data.put("validFor", "1y");
            if (hasDisabledField) {
                data.put("disabled", false);
            }
            if (hasStudentField) {
                data.put("student", false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post("/tickets")
                .then().extract().response();

        Boolean disabledResponse = response.jsonPath().getBoolean("disabled");
        Boolean studentResponse = response.jsonPath().getBoolean("student");

        Assertions.assertEquals(false, disabledResponse);
        Assertions.assertEquals(false, studentResponse);
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "2022-05-16T14:12",
                    "2022-05-16T14:12:00.123",
                    "2022-05-16T14:12:00.123456",
                    "2022-05-16T14:12:00.123456789",
            }
    )
    void whenBadValidationDateFormat_thenNotValid(String date) {

        TicketRequest ticketRequest = new TicketRequest().validFrom(date)
                .birthdate("1999-09-19").validFor(TicketRequest.ValidForEnum._1Y);

        sendAndAssert400(ticketRequest);
    }

    @ParameterizedTest
    @MethodSource("provideAllowedTicketRequests")
    void whenAllowedCombination_thenTicketOkay(TicketRequest ticketRequest) {

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(ticketRequest)
                .when()
                .post("/tickets")
                .then()
                .assertThat().statusCode(describedAs(
                        "for " + ticketRequest.toString() + " should result in <201>", is(201)))
                .body("validFrom", equalTo(ticketRequest.getValidFrom()))
                .body("birthdate", equalTo(ticketRequest.getBirthdate()))
                .body("validFor", equalTo(ticketRequest.getValidFor().toString()))
                .body("disabled", equalTo(ticketRequest.getDisabled()))
                .body("discountCard", equalTo(ticketRequest.getDiscountCard()))
                .body("student", equalTo(ticketRequest.getStudent()))
                .body("zone", ticketRequest.getValidFor() == TicketRequest.ValidForEnum._1H
                        ? equalTo(ticketRequest.getZone().toString()) : equalTo(null));
    }

    @ParameterizedTest
    @MethodSource("provideDisallowedTicketRequests")
    void whenDisallowedCombination_thenTicketNotOkay(TicketRequest ticketRequest) {

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(ticketRequest)
                .when()
                .post("/tickets")
                .then()
                .assertThat().statusCode(describedAs(
                        "for " + ticketRequest.toString() + " should result in <400>", is(400)));
    }

    @Test
    void whenMalformedRequest_thenTicketRejected() {

        // 1h without Zone
        TicketRequest ticketRequest = new TicketRequest().validFrom("2022-05-16T14:13:12")
                .birthdate("1999-09-19").validFor(TicketRequest.ValidForEnum._1H);

        sendAndAssert400(ticketRequest);


        // 1h with bad Zone
        String[] params = {"2022-05-16T14:13:12", "1999-09-19", "1h", "D"};
        JSONObject data = new JSONObject();
        try {
            data.put("validFrom", params[0]);
            data.put("birthdate", params[1]);
            data.put("validFor", params[2]);
            data.put("zone", params[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data);


        // bad validFrom
        ticketRequest = new TicketRequest().validFrom("2022-16-05T16:13:12")
                .birthdate("1999-09-19").validFor(TicketRequest.ValidForEnum._30D);

        sendAndAssert400(ticketRequest);


        // bad validFor
        String[] params1 = {"2022-05-16T14:13:12", "1999-09-19", "20d", "A"};
        JSONObject data1 = new JSONObject();
        try {
            data1.put("validFrom", params1[0]);
            data1.put("birthdate", params1[1]);
            data1.put("validFor", params1[2]);
            data1.put("zone", params1[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data1);


        // int for bool
        String[] params2 = {"2022-05-16T14:13:12", "1999-09-19", "1y", "5"};
        JSONObject data2 = new JSONObject();
        try {
            data2.put("validFrom", params2[0]);
            data2.put("birthdate", params2[1]);
            data2.put("validFor", params2[2]);
            data2.put("disabled", params2[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data2);


        // string for bool
        String[] params3 = {"2022-05-16T14:13:12", "1999-09-19", "1y", "halloIbims1Student"};
        JSONObject data3 = new JSONObject();
        try {
            data3.put("validFrom", params3[0]);
            data3.put("birthdate", params3[1]);
            data3.put("validFor", params3[2]);
            data3.put("student", params3[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data3);


        // bad birthdate
        String[] params4 = {"2022-05-16T14:13:12", "1999-09-19T14:13:12", "1d", "A"};
        JSONObject data4 = new JSONObject();
        try {
            data4.put("validFrom", params4[0]);
            data4.put("birthdate", params4[1]);
            data4.put("validFor", params4[2]);
            data4.put("zone", params4[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data4);

        // bad discount card
        String[] params5 = {"2022-05-16T14:13:12", "1999-09-19", "1y", "100"};
        JSONObject data5 = new JSONObject();
        try {
            data5.put("validFrom", params5[0]);
            data5.put("birthdate", params5[1]);
            data5.put("validFor", params5[2]);
            data5.put("discountCard", params5[3]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAndAssert400(data4);

    }

    void sendAndAssert400(JSONObject data) {
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post("/tickets")
                .then()
                .assertThat().statusCode(400);
    }

    void sendAndAssert400(TicketRequest ticketRequest) {
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(ticketRequest)
                .when()
                .post("/tickets")
                .then()
                .assertThat().statusCode(400);
    }
}
