package de.rwth.swc.sqa;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class DiscountFindingIntegrationTest {

    private static final String CUSTOMER_CREATION_PATH = "/customers/";

    private Long customer1;
    private Long customer2;

    @LocalServerPort
    private int port;

    Long createCustomer(Customer customer) {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(customer)
                .when()
                .post(CUSTOMER_CREATION_PATH)
                .then()
                .extract().response();
        return response.jsonPath().getLong("id");
    }

    Long createDiscountCard(DiscountCard card, Long customerID) {
        // create discount card
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(card)
                .when()
                .post("/customers/" + customerID + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();
        return response.jsonPath().getLong("id");
    }

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;

        customer1 = createCustomer(new Customer().birthdate("2010-05-01").disabled(false));
        customer2 = createCustomer(new Customer().birthdate("1930-05-02").disabled(true));
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "25,1992-01-01,30d",
                    "50,2000-05-12,30d",
                    "25,2010-12-31,1y",
                    "50,2030-07-19,1y",
            }
    )
    void whenCreatingDiscountCards_thenCardsAreFoundAsExpected(int type, String validFrom, String validFor) {
        DiscountCard discountCard = new DiscountCard()
                .customerId(customer1)
                .type(type)
                .validFrom(validFrom)
                // set validity enum value according to type string
                .validFor(Enum.valueOf(DiscountCard.ValidForEnum.class, "_" + validFor.toUpperCase()));

        // create discount card
        Long cardID = createDiscountCard(discountCard, customer1);

        // retrieve the card of the customer
        Response cardGetResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        // Card ID matches the returned ID in the creation post request
        Assertions.assertEquals(cardID, cardGetResponse.jsonPath().getLong("[0].id"));
        // Card properties match requested card
        Assertions.assertEquals(customer1, cardGetResponse.jsonPath().getLong("[0].customerId"));
        Assertions.assertEquals(type, cardGetResponse.jsonPath().getInt("[0].type"));
        Assertions.assertEquals(validFrom, cardGetResponse.jsonPath().getString("[0].validFrom"));
        Assertions.assertEquals(validFor, cardGetResponse.jsonPath().getString("[0].validFor"));
    }

    @Test
    void whenRetrievingDiscountCards_thenCardsHaveCorrectOrder() {
        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1)
                .type(50)
                .validFrom("2022-02-15")
                .validFor(DiscountCard.ValidForEnum._1Y);

        DiscountCard discountCard2 = new DiscountCard()
                .customerId(customer1)
                .type(25)
                .validFrom("2023-02-15")
                .validFor(DiscountCard.ValidForEnum._30D);

        DiscountCard discountCard3 = new DiscountCard()
                .customerId(customer1)
                .type(25)
                .validFrom("1999-11-26")
                .validFor(DiscountCard.ValidForEnum._1Y);

        // create discount card
        Long card1ID = createDiscountCard(discountCard1, customer1);
        Long card2ID = createDiscountCard(discountCard2, customer1);
        Long card3ID = createDiscountCard(discountCard3, customer1);

        // retrieve the card of the customer
        Response cardGetResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        // Cards should be ordered according to their validFrom field
        // Correct order: discountCard3 , discountCard1, discountCard2
        Assertions.assertEquals(card3ID, cardGetResponse.jsonPath().getLong("[0].id"));
        Assertions.assertEquals(card1ID, cardGetResponse.jsonPath().getLong("[1].id"));
        Assertions.assertEquals(card2ID, cardGetResponse.jsonPath().getLong("[2].id"));
    }

    @Test
    void whenRetrievingDiscountCards_thenNoCardsOfOtherCustomersAreReturned() {
        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1)
                .type(25)
                .validFrom("2022-01-12")
                .validFor(DiscountCard.ValidForEnum._1Y);

        DiscountCard discountCard2 = new DiscountCard()
                .customerId(customer2)
                .type(25)
                .validFrom("2023-09-08")
                .validFor(DiscountCard.ValidForEnum._30D);

        // create discount card
        Long card1ID = createDiscountCard(discountCard1, customer1);
        Long card2ID = createDiscountCard(discountCard2, customer2);

        // retrieve the card of the customer 1
        Response cardGetResponseCustomer1 = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        // retrieve the card of the customer 2
        Response cardGetResponseCustomer2 = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + customer2 + "/discountcards")
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        // Cards should be we the only card of each customer
        Assertions.assertEquals(card1ID, cardGetResponseCustomer1.jsonPath().getLong("[0].id"));
        Assertions.assertEquals(1, cardGetResponseCustomer1.jsonPath().getList("").size());
        Assertions.assertEquals(card2ID, cardGetResponseCustomer2.jsonPath().getLong("[0].id"));
        Assertions.assertEquals(1, cardGetResponseCustomer2.jsonPath().getList("").size());
    }

    @Test
    void whenFindingDiscountCards_thenInvalidIDsInURLAreRejected() {
        // retrieve the cards of invalid customer ID
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/customer-423/discountcards")
                .then()
                .assertThat().statusCode(400);
    }

    @Test
    void whenFindingDiscountCards_thenRequestForCustomerWithoutDiscountCardIsRejected() {
        // retrieve the card of freshly created customer 1
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(404);
    }

    @Test
    void whenFindingDiscountCards_thenRequestForNonExistingCustomerIsRejected() {
        long nonExistingID = customer2 + 100;
        // retrieve the card of non-existing customer
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/customers/" + nonExistingID + "/discountcards")
                .then()
                .assertThat().statusCode(404);
    }
}
