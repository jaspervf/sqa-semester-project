package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class CustomerIntegrationTest {
    private static final String PATH = "/customers/";

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    void whenCreatingCustomer_thenValuesAsExpected() {
        JSONObject data = new JSONObject();

        try {
            data.put("birthdate", "2000-10-01");
            data.put("disabled", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(201)
                .body("birthdate", equalTo("2000-10-01"))
                .body("disabled", equalTo(true));
    }

    @Test
    void whenCreatingCustomer_thenSetIdNotAllowed() {
        JSONObject data = new JSONObject();

        try {
            data.put("birthdate", "2000-10-01");
            data.put("id", 123);
            data.put("disabled", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(400);
    }

    @Test
    void whenCreatingCustomer_invalidValuesAreRejected() {
        JSONObject data = new JSONObject();
        try {
            data.put("birthdate", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(400);

        data = new JSONObject();
        try {
            data.put("birthdate", "2022-02-29");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(400);

        data = new JSONObject();
        try {
            data.put("disabled", "sometimes");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(400);
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "2020-10-29T10:38:59",
                    "01.12.2022",
                    "01/21/2016",
            }
    )
    void whenCreatingCustomer_thenInvalidDateFormatsAreRejected(String date) {
        JSONObject data = new JSONObject();
        try {
            data.put("birthdate", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(400);
    }

    @Test
    void whenCreatingCustomer_thenDisabledDefaultsToFalse() {
        JSONObject data = new JSONObject();

        try {
            data.put("birthdate", "2000-10-01");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(PATH)
                .then()
                .assertThat().statusCode(201)
                .body("birthdate", equalTo("2000-10-01"))
                .body("disabled", equalTo(false));
    }
}