package de.rwth.swc.sqa;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.describedAs;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class TicketValidationIntegrationTest {

    String POSTVALUE = "/tickets/validate";
    String CUSTOMERPOST = "/customers/";
    String TICKETSPOST = "/tickets";
    @LocalServerPort
    private int port;

    private static Collection<Arguments> provideValidHourTimes() {

        List<Arguments> args = new ArrayList<>();
        String DATE_STAMP = "2022-05-16T14:";
        String[] timeValues = {"00", "01", "30", "59"};

        for (String timeValue : timeValues) {
            for (String value : timeValues) {
                String TimeStamp = DATE_STAMP + timeValue + ":" + value;
                args.add(Arguments.of(TimeStamp));
            }
        }
        return args;
    }

    public static Collection<Arguments> provideMatchingZone() {

        List<Arguments> args = new ArrayList<>();
        String[] Zones = {"A", "B", "C"};
        for (int i = 0; i < Zones.length; i++) {
            for (int j = 0; j <= i; j++) {
                args.add(Arguments.of(Zones[i], Zones[j]));
            }
        }
        return args;
    }

    private static Collection<Arguments> provideTicketTimeout() {
        List<Arguments> args = new ArrayList<>();
        String[] timePossibilities = {"1h", "1d", "30d", "1y"};
        // Use timestamp with seconds to avoid seconds missing in string
        String currentCheckTime = "2020-02-11T11:00:01";

        String[] timeOutPossibilities = {
                LocalDateTime.parse(currentCheckTime).plusHours(1).toString(),
                LocalDateTime.parse(currentCheckTime).plusDays(1).toString(),
                LocalDateTime.parse(currentCheckTime).plusDays(30).toString(),
                LocalDateTime.parse(currentCheckTime).plusYears(1).toString(),
        };
        for (int i = 0; i < timePossibilities.length; i++) {
            for (int j = i; j < timeOutPossibilities.length; j++) {
                args.add(Arguments.of(currentCheckTime, timePossibilities[i], timeOutPossibilities[j]));
            }
        }

        return args;
    }

    public static Collection<Arguments> provideIncorrectZone() {

        List<Arguments> args = new ArrayList<>();

        String[] zones = {"A", "B", "C"};
        for (int i = 0; i < zones.length; i++) {
            for (int j = i + 1; j < zones.length; j++) {
                args.add(Arguments.of(zones[i], zones[j]));
            }
        }
        return args;
    }

    public static Collection<Arguments> provideDiscountCardTimeout() {
        List<Arguments> args = new ArrayList<>();

        String[] timePossibilities = {"30d", "1y"};
        String discountCardTime = "2020-02-01T11:00:00";
        String currentCheckTimePlusOneSec = "2020-02-01T11:00:01";
        String ticketValidFrom = "2020-03-01T11:00:00";
        String customerBirthdate = "1992-01-01";
        String[] timeOutPossibilities = {LocalDateTime.parse(currentCheckTimePlusOneSec).plusDays(30).toString(),
                LocalDateTime.parse(currentCheckTimePlusOneSec).plusYears(1).toString()
        };

        for (int i = 0; i < timePossibilities.length; i++) {
            for (int j = i; j < timeOutPossibilities.length; j++) {
                args.add(Arguments.of(
                        ticketValidFrom, discountCardTime, customerBirthdate, timePossibilities[i], timeOutPossibilities[j]));
            }
        }
        return args;
    }

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    Long createCustomer(Customer customer) {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(customer)
                .when()
                .post(CUSTOMERPOST)
                .then()
                .extract().response();
        return response.jsonPath().getLong("id");
    }

    Long createDiscountCard(DiscountCard card, Long customerID) {
        // create discount card
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(card)
                .when()
                .post("/customers/" + customerID + "/discountcards")
                .then()
                .extract().response();
        return response.jsonPath().getLong("id");
    }


    //{"2022-05-16T14:00:00", "2000-10-20", "1h", false, null, FIXED_ZONE, false}
    private long buyCustomTicket(String validFrom, String birthday, String validFor, boolean disabled,
                                 Integer discountCard, String zone, boolean student) {

        JSONObject data = new JSONObject();

        try {
            data.put("validFrom", validFrom);
            data.put("birthdate", birthday);
            data.put("validFor", validFor);
            data.put("disabled", disabled);
            data.put("discountCard", discountCard);
            data.put("zone", zone);
            data.put("student", student);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(data.toString())
                .when()
                .post(TICKETSPOST);

        return response.jsonPath().getLong("id");
    }

    @ParameterizedTest
    @MethodSource("provideValidHourTimes")
    void whenWithinUnderHourAfterBuy_thenAlwaysCorrect(String timestamp) {

        //buy example Ticket without optionals
        final String FIXED_ZONE = "A";

        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "1h",
                false,
                null,
                FIXED_ZONE,
                false);

        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", FIXED_ZONE);
            ticketValidationRequest_data.put("date", timestamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "Timestamp " + timestamp);
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "2022-05-16T14:12",
                    "2022-05-16T14:12:00.123",
                    "2022-05-16T14:12:00.123456",
                    "2022-05-16T14:12:00.123456789",
            }
    )
    void whenBadValidationDateFormat_thenNotValid(String timestamp) {

        //buy example Ticket without optionals
        final String FIXED_ZONE = "A";

        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "1h",
                false,
                null,
                FIXED_ZONE,
                false);

        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", FIXED_ZONE);
            ticketValidationRequest_data.put("date", timestamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestMalformed(ticketValidationRequest_data.toString(), "Timestamp " + timestamp);
    }

    @ParameterizedTest
    @MethodSource("provideMatchingZone")
    void whenZoneMatch_thenCorrect(String ticketZone, String targetZone) {

        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "1h",
                false,
                null,
                ticketZone,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", targetZone);
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), targetZone + " is included in " + ticketZone);
    }

    @Test
    void whenStudentMatch_thenCorrect() {
        // if a student buys a (non student exclusive)-ticket then the request is still fine
        // Ticket.student = false, TicketValidationRequest.student = true is ok

        //Case 1: Ticket.student = false, Request.Student = false
        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("student", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "students boolean match (false,false)");

        //Case 2: Ticket.student = false, Request.Student = true
        ticketValidationRequest_data = new JSONObject();

        ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                false
        );
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("student", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "students boolean match (false,true)");

        //Case 3: Ticket.student = true, Request.Student = true
        ticketValidationRequest_data = new JSONObject();

        ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("student", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "students boolean match (true,true)");
    }

    @Test
    void whenDisabledMatch_thenCorrect() {
        // if a disabled person buys a (non-disabled exclusive)-ticket then the request is still fine
        // Ticket.disabled = false, TicketValidationRequest.disabled = true is ok

        //Case 1: Ticket.disabled = false, Request.disabled = false
        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("disabled", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "disabled boolean match (false,false)");

        //Case 2: Ticket.disabled = false, Request.disabled = true
        ticketValidationRequest_data = new JSONObject();

        ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                false
        );
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("disabled", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "disabled boolean match (false,true)");

        //Case 3: Ticket.disabled = true, Request.disabled = true
        ticketValidationRequest_data = new JSONObject();

        ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                true,
                null,
                null,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
            ticketValidationRequest_data.put("disabled", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "disabled boolean match (true,true)");
    }

    @Test
    void whenStudentFlagTrueAndStudentYoungerThan28_thenCorrect() {
        JSONObject ticketValidationRequest_data = new JSONObject();
        //ticket bought before 28th birthday results in discount if student flag is set

        long ticketId = buyCustomTicket(
                "2028-10-19T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2028-10-19T14:01:00");
            ticketValidationRequest_data.put("student", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "student discount is used one day before 28 birthday");

        //ticket bought using student discount and customer being 1 day old is okay
        ticketValidationRequest_data = new JSONObject();

        ticketId = buyCustomTicket(
                "2000-10-21T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2000-10-21T14:01:00");
            ticketValidationRequest_data.put("student", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "student is one day old");
    }

    //invalid tests onward

    @ParameterizedTest
    @ValueSource(ints = {25, 50})
    void whenTicketDiscountCardTrueAndHasDiscountCard_thenCorrect(int discountCardValue) {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        //create fitting discountCard for customer

        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1Id)
                .validFor(DiscountCard.ValidForEnum._30D)
                .type(discountCardValue)
                .validFrom("2022-05-16");

        long discountCard1Id = createDiscountCard(discountCard1, customer1Id);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                birthdayTimestamp,
                "30d",
                false,
                discountCardValue,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
            ticketValidationRequest_data.put("discountCardId", discountCard1Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "Has discountCard and used discountCard");
    }

    @ParameterizedTest
    @CsvSource({"A", "B", "C"})
    void whenTicketHasNoZoneButTicketValidationRequestHas_thenCorrect(String validationZone) {

        long ticketId = buyCustomTicket(
                "2022-05-16T14:00:00",
                "2000-10-20",
                "30d",
                false,
                null,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", validationZone);
            ticketValidationRequest_data.put("date", "2022-05-16T14:01:00");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(
                ticketValidationRequest_data.toString(),
                "Ticket without a zone is valid in any zone"
        );


    }

    void sendTicketValidationRequestValid(String datastream, String msg) {
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(datastream)
                .when()
                .post(POSTVALUE)
                .then()
                .assertThat().statusCode(
                        describedAs(msg + " should result in <200>",
                                is(200)));
    }

    void sendTicketValidationRequestInvalid(String datastream, String msg) {
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(datastream)
                .when()
                .post(POSTVALUE)
                .then()
                .assertThat().statusCode(describedAs(msg + " should result in <403>", is(403)));
    }

    void sendTicketValidationRequestMalformed(String datastream, String msg) {
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(datastream)
                .when()
                .post(POSTVALUE)
                .then()
                .assertThat().statusCode(describedAs(msg + " should result in <400>", is(400)));
    }

    @Test
    void whenTicketIdOutOfBounds_thenReject() {
        JSONObject ticketValidationRequest_data = new JSONObject();
        String validFromDate = "2020-01-01T12:00:00";
        long ticketId = buyCustomTicket(
                validFromDate,
                "1992-01-01",
                "1h",
                false,
                null,
                "A",
                false);
        long nextHigherTicketId = ticketId + 1L;
        try {
            ticketValidationRequest_data.put("ticketId", nextHigherTicketId); //hopefully invalid
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", validFromDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket validation with invalid TicketID");
    }

    @Test
    void whenTimeOutOfBounds_thenReject() {
        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                "2022-03-12T12:00:00",
                "1992-01-01",
                "1h",
                false,
                null,
                "A",
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2020-02-30T12:00:00");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket validation with invalid Time");
    }

    @Test
    void whenDiscountCardIdOutOfBounds_thenReject() {
        JSONObject ticketValidationRequest_data = new JSONObject();
        String customerBirthday = "1992-01-01";
        String validFromDate = "2020-01-01T12:00:00";
        long ticketId = buyCustomTicket(
                validFromDate,
                customerBirthday,
                "1y",
                false,
                25,
                null,
                false);

        Customer customer = new Customer().birthdate(customerBirthday).disabled(false);
        long customerId = createCustomer(customer);

        DiscountCard discountCard = new DiscountCard()
                .customerId(customerId)
                .type(25)
                .validFrom(LocalDate.now().toString())
                .validFor(DiscountCard.ValidForEnum._1Y);
        long discountCardId = createDiscountCard(discountCard, customerId);
        long nextHigherDiscountCardId = discountCardId + 1L;

        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", validFromDate);
            ticketValidationRequest_data.put("discountCardId", nextHigherDiscountCardId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket validation with invalid DiscountCardId");
    }

    @ParameterizedTest
    @MethodSource("provideTicketTimeout")
    void whenTicketTimeout_thenReject(String currentCheckTime, String ticketTime, String validationTime) {

        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                currentCheckTime,
                "1992-01-01",
                ticketTime,
                false,
                null,
                // set zones only on 1h tickets
                ticketTime.equals("1h") ? "A" : null,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", validationTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(),
                ticketTime + " Ticket expired for " + validationTime);

    }

    @Test
    void whenTicketNotValidYet_thenReject() {
        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2020-02-11T11:00:00",
                "1992-01-01",
                "1h",
                false,
                null,
                "A",
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2020-02-11T10:59:59");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket does not work yet");
    }

    @Test
    void whenCustomerBirthdayDiffersFromTicket_thenReject() {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        //create fitting discountCard for customer

        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1Id)
                .validFor(DiscountCard.ValidForEnum._30D)
                .type(25)
                .validFrom("2022-05-16");

        long discountCard1Id = createDiscountCard(discountCard1, customer1Id);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                "2000-10-21",
                "30d",
                false,
                25,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
            ticketValidationRequest_data.put("discountCardId", discountCard1Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Birthday mismatch in Customer and Ticket");
    }

    @Test
    void whenDisableFlagOnTicketButNotOnRequest_thenReject() {
        //configuration invalid is ticket.disabled = true, ticketRequest.disabled = false
        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2020-02-11T11:00:00",
                "1992-01-01",
                "1h",
                true,
                null,
                "A",
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2020-02-11T11:01:59");
            ticketValidationRequest_data.put("disabled", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket is for disabled but customer is not");
    }

    @Test
    void whenStudentFlagOnTicketButNotOnRequest_thenReject() {
        //configuration invalid is ticket.student = true, ticketRequest.student = false
        //ticket.student = false, ticketRequest.student = true
        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2018-02-11T11:00:00",
                "1992-01-01",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2018-02-11T11:01:59");
            ticketValidationRequest_data.put("student", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Ticket is for student but customer is not");
    }

    @ParameterizedTest
    @MethodSource("provideIncorrectZone")
    void whenIncorrectZone_thenReject(String ticketZone, String validationZone) {

        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                "2020-02-11T11:00:00",
                "1992-01-01",
                "1h",
                false,
                null,
                ticketZone,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", validationZone);
            ticketValidationRequest_data.put("date", "2020-02-11T11:01:59");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), validationZone + " is not included in " + ticketZone);

    }

    @Test
    void whenStudentIsTooOld_thenReject() {

        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2020-02-11T11:00:00",
                "1992-01-01",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2020-02-11T11:01:59");
            ticketValidationRequest_data.put("student", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Customer is not Student Anymore");
    }

    @Test
    void whenStudentIsTooOldButHasStudentStatus_thenReject() {

        JSONObject ticketValidationRequest_data = new JSONObject();
        long ticketId = buyCustomTicket(
                "2018-05-01T10:00:00",
                "1990-05-15",
                "30d",
                false,
                null,
                null,
                true);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "C");
            ticketValidationRequest_data.put("date", "2018-05-15T00:00:00");
            ticketValidationRequest_data.put("student", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Customer is too old to be a student");
    }

    @ParameterizedTest
    @MethodSource("provideDiscountCardTimeout")
    void whenDiscountCardTimeout_thenReject(String ticketValidFrom, String discountCardTime, String customerBirthdate, String timePossibility,
                                            String timeOutPossibility) {
        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                ticketValidFrom,
                customerBirthdate,
                "1y",
                false,
                25,
                null,
                false);

        Customer customer = new Customer().birthdate(customerBirthdate).disabled(false);
        long customerId = createCustomer(customer);

        DiscountCard discountCard = new DiscountCard()
                .customerId(customerId)
                .type(25)
                .validFrom(LocalDate.now().toString())
                .validFor(DiscountCard.ValidForEnum.fromValue(timePossibility));
        long discountCardId = createDiscountCard(discountCard, customerId);

        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", timeOutPossibility);
            ticketValidationRequest_data.put("discountCardId", discountCardId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(),
                "Start-time: [" + discountCardTime + "|" + timePossibility + "] DiscountCard expired for " + timeOutPossibility);

    }

    @Test
    void whenDiscountCardIsProvidedButDoesNotExist_thenReject() {
        JSONObject ticketValidationRequest_data = new JSONObject();

        long ticketId = buyCustomTicket(
                "2020-02-11T11:00:00",
                "1992-01-01",
                "30d",
                false,
                25,
                null,
                false);
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2020-02-11T11:01:59");
            ticketValidationRequest_data.put("discountCardId", Long.MAX_VALUE - 42);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(
                ticketValidationRequest_data.toString(),
                "Customer must possess an existing discount card");
    }


    //Check, whether discount card gets rejected, when expected value is 50 but is actually 25
    @Test
    void whenTicketDiscountCardHasCorrectButUnexpectedValue_thenReject() {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        //create fitting discountCard for customer

        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1Id)
                .validFor(DiscountCard.ValidForEnum._30D)
                .type(25)
                .validFrom("2022-05-16");

        long discountCard1Id = createDiscountCard(discountCard1, customer1Id);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                birthdayTimestamp,
                "30d",
                false,
                50,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
            ticketValidationRequest_data.put("discountCardId", discountCard1Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Has ticket with discountCard with type 50 but discountCard has type 25");
    }

    @Test
    void whenTicketIsBoughtWithDiscountCardButNoneIsGiven_thenReject() {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                birthdayTimestamp,
                "30d",
                false,
                25,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestInvalid(ticketValidationRequest_data.toString(), "Has ticket bought with discountCard but customer has no discountCard");

    }

    @Test
    void whenTicketIsBoughtWithoutDiscountCardButLegitOneIsGiven_thenOkay() {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        //create fitting discountCard for customer

        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1Id)
                .validFor(DiscountCard.ValidForEnum._30D)
                .type(25)
                .validFrom("2022-05-16");

        long discountCard1Id = createDiscountCard(discountCard1, customer1Id);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                birthdayTimestamp,
                "30d",
                false,
                null,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
            ticketValidationRequest_data.put("discountCardId", discountCard1Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "It is okay if legit discount card is in validation even if ticket bought without card.");
        // https://moodle.rwth-aachen.de/mod/forum/discuss.php?d=160091#p259641
    }

    //Buy Ticket with discountCard 25, while discountCard with type 50 is present at validation
    @Test
    void whenTicketIsBoughtWithLowerDiscountCardButHigherOneIsPresent_thenCorrect() {
        //create sample customer
        String birthdayTimestamp = "2000-10-20";
        Customer customer1 = new Customer().birthdate(birthdayTimestamp).disabled(false);
        long customer1Id = createCustomer(customer1);

        //create fitting discountCard for customer

        DiscountCard discountCard1 = new DiscountCard()
                .customerId(customer1Id)
                .validFor(DiscountCard.ValidForEnum._30D)
                .type(50)
                .validFrom("2022-05-16");

        long discountCard1Id = createDiscountCard(discountCard1, customer1Id);

        // create fitting ticket for customer (has discount card)
        long ticketId = buyCustomTicket(
                "2022-05-16T14:01:00",
                birthdayTimestamp,
                "30d",
                false,
                25,
                null,
                false);

        // build ticketValidateRequest
        JSONObject ticketValidationRequest_data = new JSONObject();
        try {
            ticketValidationRequest_data.put("ticketId", ticketId);
            ticketValidationRequest_data.put("zone", "A");
            ticketValidationRequest_data.put("date", "2022-05-16T14:02:00");
            ticketValidationRequest_data.put("discountCardId", discountCard1Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendTicketValidationRequestValid(ticketValidationRequest_data.toString(), "Has ticket with discountCard with type 25 and discountCard has type 50.");
    }
}