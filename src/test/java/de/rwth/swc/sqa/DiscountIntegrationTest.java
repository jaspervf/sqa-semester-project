package de.rwth.swc.sqa;

import de.rwth.swc.sqa.model.Customer;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class DiscountIntegrationTest {

    private static final String CUSTOMER_CREATION_PATH = "/customers/";

    private Long customer1;
    private Long customer2;

    @LocalServerPort
    private int port;

    Long createCustomer(Customer customer) {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(customer)
                .when()
                .post(CUSTOMER_CREATION_PATH)
                .then()
                .extract().response();
        return response.jsonPath().getLong("id");
    }

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;

        customer1 = createCustomer(new Customer().birthdate("2010-05-01").disabled(false));
        customer2 = createCustomer(new Customer().birthdate("1930-05-02").disabled(true));
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "25,1992-01-01,30d",
                    "50,2000-05-12,30d",
                    "25,2010-12-31,1y",
                    "50,2030-07-19,1y",
            }
    )
    void whenCreatingDiscountCards_thenCardsAreAsExpected(int type, String validFrom, String validFor) {
        JSONObject request_data = new JSONObject();

        try {
            request_data.put("customerId", customer1);
            request_data.put("type", type);
            request_data.put("validFrom", validFrom);
            request_data.put("validFor", validFor);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Response card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Assertions.assertEquals(customer1, card_response.jsonPath().getLong("customerId"));
        Assertions.assertEquals(type, card_response.jsonPath().getInt("type"));
        Assertions.assertEquals(validFrom, card_response.jsonPath().getString("validFrom"));
        Assertions.assertEquals(validFor, card_response.jsonPath().getString("validFor"));
    }


    @Test
    void whenCreatingDiscountCards_consecutiveCardsArePossible() {
        JSONObject request_data = new JSONObject();

        // add first card to second customer
        try {
            request_data.put("customerId", customer2);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-05-02");
            request_data.put("validFor", "1y");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer2 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        // add second valid discount card immediately following the first one of second customer
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer2);
            request_data.put("type", 50);
            request_data.put("validFrom", "2023-05-02");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer2 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();
    }

    @Test
    void whenCreatingDiscountCards_consecutiveCardsArePossibleLeapYears() {
        JSONObject request_data = new JSONObject();

        // add first card to second customer
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 25);
            request_data.put("validFrom", "2000-01-29");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        // add second valid discount card immediately following the first one of second customer
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2000-02-29");
            request_data.put("validFor", "1y");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();
    }

    @Test
    void whenCreatingDiscountCards_CardsHaveUniqueIDs() {
        JSONObject request_data = new JSONObject();

        // add first card to second customer
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2008-01-29");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Response card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Long discountCardId = card_response.jsonPath().getLong("id");


        // add second valid discount card
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-10-29");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Long discountCardId2 = card_response.jsonPath().getLong("id");

        // add second valid discount card to different customer
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer2);
            request_data.put("type", 25);
            request_data.put("validFrom", "2025-01-29");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer2 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Long discountCardId3 = card_response.jsonPath().getLong("id");

        Assertions.assertNotEquals(discountCardId, discountCardId2);
        Assertions.assertNotEquals(discountCardId, discountCardId3);
        Assertions.assertNotEquals(discountCardId2, discountCardId3);
    }

    @Test
    void whenCreatingDiscountCards_TwoCustomersCanHaveDistinctCardsWithSameProperties() {
        JSONObject request_data = new JSONObject();

        // add first card to first customer
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-11-15");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Response card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Long discountCardId = card_response.jsonPath().getLong("id");


        // add second card to other customer
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer2);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-11-15");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        card_response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer2 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();

        Long discountCardId2 = card_response.jsonPath().getLong("id");

        Assertions.assertNotEquals(discountCardId, discountCardId2);
    }

    @Test
    void whenCreatingDiscountCards_thenInvalidInputGetsRejected() {
        JSONObject request_data = new JSONObject();

        // add card for customer 1 with invalid customerId type
        try {
            request_data.put("customerId", true);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with invalid "type" type
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", "Oh yes!");
            request_data.put("validFrom", "2022-05-02");
            request_data.put("validFor", "1y");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with invalid "validFrom" type
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", "Oh yes!");
            request_data.put("validFrom", 20220502);
            request_data.put("validFor", "1y");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with invalid "validFor" type
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", "Oh yes!");
            request_data.put("validFrom", "2022-05-02");
            request_data.put("validFor", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();

    }

    @Test
    void whenCreatingDiscountCards_thenOOBInputGetsRejected() {
        JSONObject request_data = new JSONObject();

        // add card for customer 1 with non-existing customerId
        Long customerID = customer2 + 100;
        try {
            request_data.put("customerId", customerID);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customerID + "/discountcards")
                .then()
                .assertThat().statusCode(404)
                .extract().response();


        // add card for customer 1 with non-existing "type"
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 30);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with non-existing validFrom/date
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-04-31");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with non-existing validFor
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-04-31");
            request_data.put("validFor", "2y");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();


        // add card for customer 1 with supplied id in card
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("id", 55);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-04-31");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();

    }

    @Test
    void whenCreatingDiscountCards_rejectIfTwoCardsForOneCustomerId() {
        JSONObject request_data = new JSONObject();

        // add card for customer 1
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 25);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(201)
                .extract().response();


        // add another card for customer 1 with different type
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(409)
                .extract().response();

        // add another card for customer 1 with different validFrom
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-05-02");
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(409)
                .extract().response();

        // add another card for customer 1 with different validFor
        request_data = new JSONObject();
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 50);
            request_data.put("validFrom", "2022-05-01");
            request_data.put("validFor", "1y");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(409)
                .extract().response();
    }

    @ParameterizedTest
    @CsvSource(
            {
                    "2020-10-29T10:38:59",
                    "01.12.2022",
                    "01/21/2016",
            }
    )
    void whenCreatingDiscountCard_thenInvalidDateFormatsAreRejected(String date) {
        JSONObject request_data = new JSONObject();

        // add card for customer 1
        try {
            request_data.put("customerId", customer1);
            request_data.put("type", 25);
            request_data.put("validFrom", date);
            request_data.put("validFor", "30d");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request_data.toString())
                .when()
                .post("/customers/" + customer1 + "/discountcards")
                .then()
                .assertThat().statusCode(400)
                .extract().response();
    }

}
