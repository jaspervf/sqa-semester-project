package de.rwth.swc.sqa.api;


import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
public class CustomerController implements CustomersApi {
    final CustomerDatabaseService customerDatabaseService;
    final DiscountCardDatabaseService discountCardDatabaseService;

    @Autowired
    public CustomerController(CustomerDatabaseService customerDatabaseService,
                              DiscountCardDatabaseService discountCardDatabaseService) {
        this.customerDatabaseService = customerDatabaseService;
        this.discountCardDatabaseService = discountCardDatabaseService;
    }

    /**
     * Calculates and returns the last day on which the inputted DiscountCard is valid.
     * Only for DiscountCards that have a valid start date string!
     *
     * @param card DiscountCard Object of which to calculate the end date
     * @return LocalDate object representing the last day the card is valid
     */
    private static LocalDate getCardLastValidDate(DiscountCard card) {
        LocalDate cardStartDate = LocalDate.parse(card.getValidFrom());
        return switch (card.getValidFor()) {
            case _30D -> cardStartDate.plusDays(29);
            case _1Y -> cardStartDate.minusDays(1).plusYears(1);
        };
    }

    /**
     * POST /customers : Add a new customer
     * Adds a new customer that can order cards.
     * The id must not be set in the request but has to be present in the response.
     * The disabled flag should default to false if not set in the request.
     * Make sure that the status codes are reported correctly.
     *
     * @param newCustomer Customer object that should be added (required)
     * @return Saved (status code 201)
     * or Invalid input (status code 400)
     */
    @Override
    public ResponseEntity<Customer> addCustomer(@ApiParam(value = "Customer object that should be added", required = true)
                                                @Valid @RequestBody Customer newCustomer) {

        if (newCustomer.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // add disabled status if none provided
        if (newCustomer.getDisabled() == null) {
            newCustomer.setDisabled(false);
        }

        try {
            LocalDate.parse(newCustomer.getBirthdate());
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // add customer to database
        customerDatabaseService.add(newCustomer);

        return new ResponseEntity<>(
                newCustomer,
                HttpStatus.CREATED
        );
    }

    /**
     * POST /customers/{customerId}/discountcards : add a discount card to a customer
     * The id field must not be set in the request but has to be present in the response.
     *
     * @param customerId ID of customer (required)
     * @param newCard    DiscountCard object that needs to be added to the customer (required)
     * @return Saved (status code 201)
     * or Invalid input (status code 400)
     * or Customer not found (status code 404)
     * or Conflict. There is a card that is valid in the same timeframe or part of it (status code 409)
     */
    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard newCard) {
        // check that customer id in card and path match, card has valid type and date is valid
        if (newCard.getId() != null || !Objects.equals(customerId, newCard.getCustomerId())
                || (newCard.getType() != 25 && newCard.getType() != 50)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        LocalDate newCardStartDate;
        try {
            newCardStartDate = LocalDate.parse(newCard.getValidFrom());
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if (!customerDatabaseService.exists(customerId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        // make sure that new card is not overlapping with existing cards in terms of time frame
        if (discountCardDatabaseService.customerHasADiscountCard(customerId)) {
            LocalDate newCardLastDate = getCardLastValidDate(newCard);

            List<DiscountCard> discountCards = discountCardDatabaseService.getListByCustomerId(customerId);
            // check for each card if its duration overlaps with current one
            for (var existingCard : discountCards) {
                LocalDate existingStart = LocalDate.parse(existingCard.getValidFrom());
                LocalDate existingEnd = getCardLastValidDate(existingCard);
                if (!(newCardLastDate.isBefore(existingStart) || newCardStartDate.isAfter(existingEnd))) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT);
                }
            }

        }
        // add card to database
        discountCardDatabaseService.add(newCard);

        return new ResponseEntity<>(newCard, HttpStatus.CREATED);
    }

    /**
     * GET /customers/{customerId}/discountcards : Find discount cards of given customer
     * Returns a list of discount cards for the given customer order by validFrom
     *
     * @param customerId ID of customer to search for discount cards (required)
     * @return successful operation (status code 200)
     * or Invalid ID supplied (status code 400)
     * or Customer doesnt exist or no card found (status code 404)
     */
    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {

        if (!customerDatabaseService.exists(customerId) || !discountCardDatabaseService.customerHasADiscountCard(customerId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        var customersCards = new ArrayList<>(discountCardDatabaseService.getListByCustomerId(customerId));

        customersCards.sort((o1, o2) -> {
            var date1 = LocalDate.parse(o1.getValidFrom());
            var date2 = LocalDate.parse(o2.getValidFrom());
            return date1.compareTo(date2);
        });

        return new ResponseEntity<>(customersCards, HttpStatus.OK);
    }

}
