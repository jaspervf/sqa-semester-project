package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Customer;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class CustomerDatabaseService implements IDatabaseService<Customer> {
    final HashMap<Long, Customer> customerDatabase = new HashMap<>();
    long nextCustomerId = 0;

    @Override
    public long add(Customer customer) {
        // add customer to database
        customer.setId(nextCustomerId);
        customerDatabase.put(nextCustomerId, customer);
        nextCustomerId++;
        return customer.getId();
    }

    @Override
    public boolean exists(long id) {
        return customerDatabase.containsKey(id);
    }

    @Override
    public Customer getById(long id) {
        return customerDatabase.get(id);
    }
}
