package de.rwth.swc.sqa.api;

public interface IDatabaseService<T> {
    long add(T model);

    boolean exists(long id);

    T getById(long id);
}
