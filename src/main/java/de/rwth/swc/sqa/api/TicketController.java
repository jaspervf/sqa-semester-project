package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Controller
public class TicketController implements TicketsApi {

    final TicketDatabaseService ticketDatabaseService;
    final DiscountCardDatabaseService discountCardDatabaseService;
    final CustomerDatabaseService customerDatabaseService;
    final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    @Autowired
    public TicketController(TicketDatabaseService ticketDatabaseService,
                            DiscountCardDatabaseService discountCardDatabaseService,
                            CustomerDatabaseService customerDatabaseService) {
        this.ticketDatabaseService = ticketDatabaseService;
        this.discountCardDatabaseService = discountCardDatabaseService;
        this.customerDatabaseService = customerDatabaseService;
    }

    private static Ticket.ValidForEnum validForEnumRequestToTicket(@NotNull TicketRequest.ValidForEnum requestEnum) {
        return switch (requestEnum) {
            case _1H -> Ticket.ValidForEnum._1H;
            case _1D -> Ticket.ValidForEnum._1D;
            case _30D -> Ticket.ValidForEnum._30D;
            case _1Y -> Ticket.ValidForEnum._1Y;
        };
    }

    private static Ticket.ZoneEnum zoneEnumRequestToTicket(TicketRequest.ZoneEnum requestEnum) {
        if (requestEnum == null) {
            return null;
        }
        return switch (requestEnum) {
            case A -> Ticket.ZoneEnum.A;
            case B -> Ticket.ZoneEnum.B;
            case C -> Ticket.ZoneEnum.C;
        };
    }

    private int getZoneRange(String zone) {
        return switch (zone) {
            case ("A") -> 1;
            case ("B") -> 2;
            case ("C") -> 3;
            default -> 0;
        };
    }

    private boolean isValidZoneSpecifier(Ticket.ZoneEnum reqZone) {
        if (reqZone == Ticket.ZoneEnum.A) return true;
        if (reqZone == Ticket.ZoneEnum.B) return true;
        return reqZone == Ticket.ZoneEnum.C;
    }

    //if student then you cannot request 1d or 1h tickets
    private boolean forbiddenStudentRequest(Ticket.ValidForEnum reqValidFor, boolean reqStudent) {
        return reqStudent && ((reqValidFor == Ticket.ValidForEnum._1D) || (reqValidFor == Ticket.ValidForEnum._1H));
    }

    private boolean forbidden1HZoneRequest(Ticket.ValidForEnum reqValidFor, Ticket.ZoneEnum reqZone) {
        return (reqValidFor == Ticket.ValidForEnum._1H) && (!isValidZoneSpecifier(reqZone));
    }

    private boolean zoneIncludedInNonZoneTicketRequest(Ticket.ValidForEnum reqValidFor, Ticket.ZoneEnum reqZone) {
        return (reqValidFor != Ticket.ValidForEnum._1H) && (isValidZoneSpecifier(reqZone));
    }

    // change given date string to LocalDate for tickets
    private LocalDateTime getEndDate(Ticket ticket) {
        LocalDateTime startDate = LocalDateTime.parse(ticket.getValidFrom(), dateTimeFormatter);
        return switch (ticket.getValidFor()) {
            case _1H -> startDate.plusHours(1);
            case _1D -> startDate.plusDays(1);
            case _30D -> startDate.plusDays(30);
            case _1Y -> startDate.plusYears(1);
        };
    }


    // change given date string to LocalDate for discountCards
    private LocalDate getEndDateDiscountCard(DiscountCard discountCard) {
        LocalDate startDate = LocalDate.parse(discountCard.getValidFrom());
        return switch (discountCard.getValidFor()) {
            case _30D -> startDate.plusDays(30);
            case _1Y -> startDate.plusYears(1);
        };
    }

    // check, whether ticket is valid at given time
    private void checkValidityOfTicket(TicketValidationRequest body) {
        Ticket origTicket = ticketDatabaseService.getById(body.getTicketId());

        try {
            LocalDateTime startDate = LocalDateTime.parse(origTicket.getValidFrom(), dateTimeFormatter);
            LocalDateTime endDate = getEndDate(origTicket);
            LocalDateTime givenDate = LocalDateTime.parse(body.getDate(), dateTimeFormatter);
            if (!(givenDate.isEqual(startDate)
                    || (givenDate.isAfter(startDate)
                    && givenDate.isBefore(endDate)))) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    // if customer has discount card, check whether it is valid
    private boolean checkValidityOfDiscountCard(TicketValidationRequest body) {
        Ticket origTicket = ticketDatabaseService.getById(body.getTicketId());
        DiscountCard origDiscountCard = discountCardDatabaseService.getById(body.getDiscountCardId());

        if (body.getDiscountCardId() != null && origTicket.getDiscountCard() != null) {
            if (!discountCardDatabaseService.exists(body.getDiscountCardId())) {
                return false;
            } else {
                if (origTicket.getDiscountCard() > origDiscountCard.getType()) {
                    return false;
                }
            }

            LocalDate startDateDiscountCard = LocalDate.parse(origDiscountCard.getValidFrom());
            LocalDate endDateDiscountCard = getEndDateDiscountCard(origDiscountCard);
            LocalDateTime givenDateTime = LocalDateTime.parse(body.getDate(), dateTimeFormatter);
            LocalDate givenDate = givenDateTime.toLocalDate();
            return givenDate.isEqual(startDateDiscountCard) || (givenDate.isAfter(startDateDiscountCard) && givenDate.isBefore(endDateDiscountCard));
        }
        return true;
    }

    //check, whether birthday via ticket -> birthday matches birthday via discountCard -> customer -> birthday
    private void checkMatchingBirthdayOnDCAndTicket(TicketValidationRequest body) {
        Ticket origTicket = ticketDatabaseService.getById(body.getTicketId());

        if (body.getDiscountCardId() != null) {
            DiscountCard origDiscountCard = discountCardDatabaseService.getById(body.getDiscountCardId());
            if (!(origTicket.getBirthdate().equals(customerDatabaseService.getById(origDiscountCard.getCustomerId()).getBirthdate()))) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        }
    }


    /**
     * POST /tickets : buy a new ticket
     * buy a new ticket. disabled, discountCard and student are optional and should default to false if not set. See Ticket table for available Tickettypes
     *
     * @param body TicketRequest object (required)
     * @return Ticket is saved (status code 201)
     * or Invalid Request. No such Ticket exists (status code 400)
     */
    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest body) {
        // validFrom is required and always present
        String reqValidFrom = body.getValidFrom();

        // check if validFrom date is correctly formatted valid
        try {
            LocalDateTime.parse(reqValidFrom, dateTimeFormatter);
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        //birthdate is required
        String reqBirthdate = body.getBirthdate();
        try {
            LocalDate.parse(reqBirthdate);
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        //validFor is required
        // there are 2 ValidFor Enums, one in Ticket and one in Ticket request. We need to convert manually
        // should be refactored
        Ticket.ValidForEnum reqValidFor = validForEnumRequestToTicket(body.getValidFor());

        //zone is null for all non-1h tickets
        //there are 2 Zone Enums, one in Ticket and one in Ticket request. We need to convert manually
        Ticket.ZoneEnum reqZone = zoneEnumRequestToTicket(body.getZone());

        //booleans are all optional, therefore set to false by default
        boolean reqDisabled = false;
        if (body.getDisabled() != null) reqDisabled = body.getDisabled();
        boolean reqStudent = false;
        if (body.getStudent() != null) reqStudent = body.getStudent();

        //DiscountCard is now Integer
        if (body.getDiscountCard() != null && body.getDiscountCard() != 25 && body.getDiscountCard() != 50) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if (forbiddenStudentRequest(reqValidFor, reqStudent)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if (forbidden1HZoneRequest(reqValidFor, reqZone)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // zone is only allowed in 1h tickets
        if (zoneIncludedInNonZoneTicketRequest(reqValidFor, reqZone)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        //onwards the ticket is valid

        Ticket newTicket = new Ticket();

        //handle ticket information
        newTicket.setValidFrom(reqValidFrom);
        newTicket.setBirthdate(reqBirthdate);
        newTicket.setValidFor(reqValidFor);
        newTicket.setZone(reqZone);

        newTicket.setDisabled(reqDisabled);
        newTicket.setDiscountCard(body.getDiscountCard());
        newTicket.setStudent(reqStudent);

        ticketDatabaseService.add(newTicket);
        return new ResponseEntity<>(newTicket, HttpStatus.CREATED);
    }


    /**
     * POST /tickets/validate : Check if a ticket is valid
     * Check if a ticket is valid given the required information to do a validation. Should be invalid if one of the ticket properties doesn&#39;t match the provided properties. Zone B includes Zone A. Furthermore Zone C includes all Zones.
     *
     * @param body TicketValidationRequest object that needs to validated (required)
     * @return Ticket is valid (status code 200)
     * or Ticket is not valid (status code 403)
     */
    @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {

        // get original ticket from ticketId
        if (!ticketDatabaseService.exists(body.getTicketId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Ticket origTicket = ticketDatabaseService.getById(body.getTicketId());

        // if zone on original ticket is null, it is valid in all zones
        if (origTicket.getZone() != null) {
            // check whether zone is the same. Zone must be present in validation request
            int origZoneRange = getZoneRange(origTicket.getZone().getValue());
            int currentZone = getZoneRange(body.getZone().getValue());
            if (origZoneRange < currentZone) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        }

        // check whether date is valid. Date is required
        checkValidityOfTicket(body);

        // check, whether customer is not disabled, but ticket is for disabled
        if (origTicket.getDisabled()
                && (body.getDisabled() == null
                || !body.getDisabled())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        // check, whether customer has no discount card, but ticket is bought with one
        if (origTicket.getDiscountCard() != null && body.getDiscountCardId() == null) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        // if customer has discount card, check whether it is valid
        if (body.getDiscountCardId() != null && !checkValidityOfDiscountCard(body)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        //check, whether customer is not marked as student, but has student ticket
        Boolean isCustomerStudent = body.getStudent();
        boolean isStudentTicket = origTicket.getStudent();
        if (isCustomerStudent != null && !isCustomerStudent && isStudentTicket) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        //check, whether birthday via ticket -> birthday matches birthday via discountCard -> customer -> birthday
        checkMatchingBirthdayOnDCAndTicket(body);

        //check, if customer is marked as student, than he/she is younger than 28
        LocalDate lastDayCustomerIsStillStudent = LocalDate.parse(origTicket.getBirthdate()).plusYears(28).minusDays(1);
        LocalDateTime timeToCheck = LocalDateTime.parse(body.getDate(), dateTimeFormatter);
        LocalDate dayToCheck = timeToCheck.toLocalDate();
        if (origTicket.getStudent()
                && dayToCheck.isAfter(lastDayCustomerIsStillStudent)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        // if all checks are through, ticket is valid
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
