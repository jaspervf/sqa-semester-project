package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class DiscountCardDatabaseService implements IDatabaseService<DiscountCard> {
    final HashMap<Long, ArrayList<DiscountCard>> discountCardByCustomerIdDatabase = new HashMap<>();
    final HashMap<Long, DiscountCard> discountCardDatabase = new HashMap<>();
    long nextDiscountCardId = 0;

    @Override
    public long add(DiscountCard discountCard) {
        discountCard.setId(nextDiscountCardId);
        discountCardDatabase.put(nextDiscountCardId, discountCard);
        nextDiscountCardId++;

        if (!this.customerHasADiscountCard(discountCard.getCustomerId())) {
            // customer has no discount cards yet
            ArrayList<DiscountCard> arrayList = new ArrayList<>();
            arrayList.add(discountCard);
            discountCardByCustomerIdDatabase.put(discountCard.getCustomerId(), arrayList);
        } else {
            // add discount card to existing cards of customer
            List<DiscountCard> arrayList = this.getListByCustomerId(discountCard.getCustomerId());
            arrayList.add(discountCard);
        }

        return discountCard.getId();
    }

    @Override
    public boolean exists(long id) {
        return discountCardDatabase.containsKey(id);
    }

    public boolean customerHasADiscountCard(long customerId) {
        return discountCardByCustomerIdDatabase.containsKey(customerId);
    }

    @Override
    public DiscountCard getById(long id) {
        return discountCardDatabase.get(id);
    }

    public List<DiscountCard> getListByCustomerId(long customerId) {
        return discountCardByCustomerIdDatabase.get(customerId);
    }
}
