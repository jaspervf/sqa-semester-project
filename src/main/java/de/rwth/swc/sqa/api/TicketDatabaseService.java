package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Ticket;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class TicketDatabaseService implements IDatabaseService<Ticket> {
    final HashMap<Long, Ticket> ticketDatabase = new HashMap<>();
    long nextTicketId = 0;

    @Override
    public long add(Ticket ticket) {
        // add ticket to database
        ticket.setId(nextTicketId);
        ticketDatabase.put(nextTicketId, ticket);
        nextTicketId++;
        return ticket.getId();
    }

    @Override
    public boolean exists(long id) {
        return ticketDatabase.containsKey(id);
    }

    @Override
    public Ticket getById(long id) {
        return ticketDatabase.get(id);
    }
}
